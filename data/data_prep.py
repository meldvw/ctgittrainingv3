def get_data(dataset):
    # random comment
    import pandas as pd
    df = pd.read_csv(dataset)
    return df

def summary(df):
    return df.describe()
